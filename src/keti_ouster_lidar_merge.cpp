#include "keti_ouster_lidar_merge/keti_ouster_lidar_merge.h"
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Point.h>

keti_ouster_lidar_merge::keti_ouster_lidar_merge()
{
  ros::NodeHandle private_nh("~");

  private_nh.param<std::string>("target_lidar_frame", target_lidar_frame_, "roof/os_sensor");
  private_nh.param<std::string>("sourcre_lidar_frame_front", source_lidar_front_frame_, "front/os_sensor");
  private_nh.param<std::string>("sourcre_lidar_frame_left", source_lidar_left_frame_, "left/os_sensor");
  private_nh.param<std::string>("sourcre_lidar_frame_right", source_lidar_right_frame_, "right/os_sensor");

  std::string target_lidar_topic_, source_lidar_topic_front_, source_lidar_topic_left_, source_lidar_topic_right_;
  private_nh.param<std::string>("target_lidar_topic", target_lidar_topic_, "/os_cloud_node_roof/points");
  private_nh.param<std::string>("source_lidar_topic_front", source_lidar_topic_front_, "/os_cloud_node_front/points");
  private_nh.param<std::string>("source_lidar_topic_left", source_lidar_topic_left_, "/os_cloud_node_left/points");
  private_nh.param<std::string>("source_lidar_topic_right", source_lidar_topic_right_, "/os_cloud_node_right/points");

  ROS_INFO("target_lidar_frame : %s", target_lidar_frame_.c_str());
  ROS_INFO("target_lidar_topic : %s", target_lidar_topic_.c_str());

  ROS_INFO("source_lidar_frame_front : %s", source_lidar_front_frame_.c_str());
  ROS_INFO("source_lidar_topic_front : %s", source_lidar_topic_front_.c_str());

  ROS_INFO("source_lidar_frame_left : %s", source_lidar_left_frame_.c_str());
  ROS_INFO("source_lidar_topic_left : %s", source_lidar_topic_left_.c_str());

  ROS_INFO("source_lidar_frame_right : %s", source_lidar_right_frame_.c_str());
  ROS_INFO("source_lidar_topic_right : %s", source_lidar_topic_right_.c_str());

  merged_lidar_pub_ = node_handle_.advertise<sensor_msgs::PointCloud2>("/merged_filterd_velodyne", 1);

  roof_subscriber_ =
      node_handle_.subscribe(target_lidar_topic_, 1,
                             &keti_ouster_lidar_merge::callback_roof, this);
  front_subscriber_ =
      node_handle_.subscribe(source_lidar_topic_front_, 1,
                             &keti_ouster_lidar_merge::callback_front, this);
  left_subscriber_ =
      node_handle_.subscribe(source_lidar_topic_left_, 1,
                             &keti_ouster_lidar_merge::callback_left, this);
  right_subscriber_ =
      node_handle_.subscribe(source_lidar_topic_right_, 1,
                             &keti_ouster_lidar_merge::callback_right, this);

  roof_pc.reset(new pcl::PointCloud<pcl::PointXYZI>);
  front_pc.reset(new pcl::PointCloud<pcl::PointXYZI>);
  left_pc.reset(new pcl::PointCloud<pcl::PointXYZI>);
  right_pc.reset(new pcl::PointCloud<pcl::PointXYZI>);

  find_transform_front = find_transform_left = find_transform_right = false;
}

keti_ouster_lidar_merge::~keti_ouster_lidar_merge()
{
}

void keti_ouster_lidar_merge::callback_roof(const sensor_msgs::PointCloud2::ConstPtr& point_cloud)
{
  if(find_transform_front && find_transform_left && find_transform_right)
  {
    pcl::fromROSMsg(*point_cloud, *roof_pc);

    if(!front_pc->points.empty() && !left_pc->points.empty() && !right_pc->points.empty())
    {
      merge_pc();
    }
  }
}

void keti_ouster_lidar_merge::callback_front(const sensor_msgs::PointCloud2::ConstPtr& point_cloud)
{
  if(!find_transform_front)
    FindTransfrom(target_lidar_frame_, source_lidar_front_frame_);

  if(find_transform_left && find_transform_right)
  {
    pcl::fromROSMsg(*point_cloud, *front_pc);

    if(!roof_pc->points.empty() && !left_pc->points.empty() && !right_pc->points.empty())
    {
      merge_pc();
    }
  }
}

void keti_ouster_lidar_merge::callback_left(const sensor_msgs::PointCloud2::ConstPtr& point_cloud)
{
  if(!find_transform_left)
    FindTransfrom(target_lidar_frame_, source_lidar_left_frame_);

  if(find_transform_front && find_transform_right)
  {
    pcl::fromROSMsg(*point_cloud, *left_pc);

    if(!roof_pc->points.empty() && !front_pc->points.empty() && !right_pc->points.empty())
    {
      merge_pc();
    }
  }
}

void keti_ouster_lidar_merge::callback_right(const sensor_msgs::PointCloud2::ConstPtr& point_cloud)
{
  if(!find_transform_right)
    FindTransfrom(target_lidar_frame_, source_lidar_right_frame_);

  if(find_transform_front && find_transform_left)
  {
    pcl::fromROSMsg(*point_cloud, *right_pc);

    if(!roof_pc->points.empty() && !front_pc->points.empty() && !left_pc->points.empty())
    {
      merge_pc();
    }
  }
}

void keti_ouster_lidar_merge::merge_pc()
{
  for(int i = 0; i < front_pc->points.size(); i++)
  {
    pcl::PointXYZI points;
    tf::Vector3 tf_point(front_pc->points[i].x, front_pc->points[i].y, front_pc->points[i].z);
    tf::Vector3 tf_point_t = front_transform * tf_point;

    points._PointXYZI::x = tf_point_t.getX();
    points._PointXYZI::y = tf_point_t.getY();
    points._PointXYZI::z = tf_point_t.getZ();
    points._PointXYZI::intensity = front_pc->points[i]._PointXYZI::intensity;

    roof_pc->points.push_back(points);
  }

  for(int i = 0; i < left_pc->points.size(); i++)
  {
    pcl::PointXYZI points;
    tf::Vector3 tf_point(left_pc->points[i].x, left_pc->points[i].y, left_pc->points[i].z);
    tf::Vector3 tf_point_t = left_transform * tf_point;

    points._PointXYZI::x = tf_point_t.getX();
    points._PointXYZI::y = tf_point_t.getY();
    points._PointXYZI::z = tf_point_t.getZ();
    points._PointXYZI::intensity = front_pc->points[i]._PointXYZI::intensity;

    roof_pc->points.push_back(points);
  }

  for(int i = 0; i < right_pc->points.size(); i++)
  {
    pcl::PointXYZI points;
    tf::Vector3 tf_point(right_pc->points[i].x, right_pc->points[i].y, right_pc->points[i].z);
    tf::Vector3 tf_point_t = right_transform * tf_point;

    points._PointXYZI::x = tf_point_t.getX();
    points._PointXYZI::y = tf_point_t.getY();
    points._PointXYZI::z = tf_point_t.getZ();
    points._PointXYZI::intensity = front_pc->points[i]._PointXYZI::intensity;

    roof_pc->points.push_back(points);
  }

  pcl_conversions::toPCL(ros::Time::now(), roof_pc->header.stamp);
  roof_pc->height = 1;
  roof_pc->width = roof_pc->points.size();
  merged_lidar_pub_.publish(roof_pc);
}

void keti_ouster_lidar_merge::FindTransfrom(const std::string &target_frame, const std::string &source_frame)
{
  ROS_INFO("%s -> %s", source_frame.c_str(), target_frame.c_str());
  try
  {
    if(source_frame == "front/os_sensor")
    {
      transform_listener_.lookupTransform(target_frame, source_frame, ros::Time(0), front_transform);
      find_transform_front = true;
      ROS_INFO("front LiDAR -> roof LiDAR TF obtained");
    }
    else if(source_frame == "left/os_sensor")
    {
      transform_listener_.lookupTransform(target_frame, source_frame, ros::Time(0), left_transform);
      find_transform_left = true;
      ROS_INFO("left LiDAR -> roof LiDAR TF obtained");
    }
    else if(source_frame == "right/os_sensor")
    {
      transform_listener_.lookupTransform(target_frame, source_frame, ros::Time(0), right_transform);
      find_transform_right = true;
      ROS_INFO("right LiDAR -> roof LiDAR TF obtained");
    }
  }
  catch (tf::TransformException &ex)
  {
    ROS_ERROR("%s", ex.what());
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "keti_ouster_lidar_merge");
  keti_ouster_lidar_merge keti_ouster_lidar_merge;
  ros::spin();
  return 0;
}
