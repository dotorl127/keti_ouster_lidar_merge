#ifndef KETI_OUSTER_LIDAR_MERGE_H
#define KETI_OUSTER_LIDAR_MERGE_H

#include <stdio.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_listener.h>

using namespace std;
using namespace pcl;

class keti_ouster_lidar_merge
{
public:
  void callback_roof(const sensor_msgs::PointCloud2::ConstPtr& point_cloud);
  void callback_front(const sensor_msgs::PointCloud2::ConstPtr& point_cloud);
  void callback_left(const sensor_msgs::PointCloud2::ConstPtr& point_cloud);
  void callback_right(const sensor_msgs::PointCloud2::ConstPtr& point_cloud);
  void FindTransfrom(const std::string &target_frame, const std::string &source_frame);
  void merge_pc();

  keti_ouster_lidar_merge();
  ~keti_ouster_lidar_merge();

private:
  ros::NodeHandle node_handle_;
  ros::Subscriber roof_subscriber_, front_subscriber_, left_subscriber_, right_subscriber_;

  pcl::PointCloud<pcl::PointXYZI>::Ptr roof_pc;
  pcl::PointCloud<pcl::PointXYZI>::Ptr front_pc;
  pcl::PointCloud<pcl::PointXYZI>::Ptr left_pc;
  pcl::PointCloud<pcl::PointXYZI>::Ptr right_pc;

  tf::StampedTransform front_transform, left_transform, right_transform;
  ros::Publisher merged_lidar_pub_;
  string target_lidar_frame_, source_lidar_front_frame_, source_lidar_left_frame_, source_lidar_right_frame_;
  tf::TransformListener transform_listener_;
  bool find_transform_front, find_transform_left, find_transform_right;
};

#endif // KETI_OUSTER_LIDAR_MERGE_H
